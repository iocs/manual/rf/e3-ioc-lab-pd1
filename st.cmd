require essioc
require xtpico,0.12.0+0

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("PREFIX",               "$(PREFIX=LabS-RFLab:RFS-PIND-110)")
epicsEnvSet("DEVICE_IP",            "rf-lab-pd02.cslab.esss.lu.se")

epicsEnvSet("I2C_COMM_PORT",        "AK_I2C_COMM")
epicsEnvSet("R_TMP100",             ":Temp")
epicsEnvSet("R_M24M02",             ":Eeprom")
epicsEnvSet("R_TCA9555",            ":IOExp")
epicsEnvSet("R_LTC2991",            ":VMon")

iocshLoad("$(xtpico_DIR)/pin-diode.iocsh")

afterInit("dbpf $(PREFIX)$(R_TCA9555)-DirPin0 0")
afterInit("dbpf $(PREFIX)$(R_TCA9555)-DirPin1 0")
afterInit("dbpf $(PREFIX)$(R_TCA9555)-LevelPin0 0")
afterInit("dbpf $(PREFIX)$(R_TCA9555)-LevelPin1 0")

iocInit()
